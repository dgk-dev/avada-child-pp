<?php

//cleanup
require(get_stylesheet_directory().'/cleanup.php');

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

function penp_allow_skype_protocol( $protocols ){
    $protocols[] = 'skype';
    return $protocols;
}
add_filter( 'kses_allowed_protocols' , 'penp_allow_skype_protocol' );


/**
 * Custom login screen
 */
add_action('wp_dashboard_setup', 'penp_dashboard_widgets');
function penp_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_stylesheet_directory_uri().'/img/PenPaperGrow-logo.png"></div><p><strong>Este es el administrador del sitio PenPaper</strong></p><p>Desde aquí podrás gestionar las entradas de blog así como consultar y exportar la información de los usuarios que llenaron el formulario de contacto<p><p>En la sección <strong>Perfil</strong> podrás editar tus datos principales y tu contraseña.';
}

add_action( 'login_enqueue_scripts', 'penp_login_logo' );
function penp_login_logo() { ?>
    <style type="text/css">
        body.login{
            background: #ffffff;
        }
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/PenPaperGrow-logo.png');
            height: 205px;
            width: 300px;
            background-size: 300px 205px;
            background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }

add_filter( 'login_headerurl', 'penp_loginlogo_url');
function penp_loginlogo_url($url) {
    return home_url();
}